#!/bin/sh
VERSION=$2
BASEDIR=$(dirname $3)

cd $BASEDIR
tar cf casacore-data-igrf_$VERSION.orig.tar igrf${VERSION}coeffs.txt
rm -f igrf${VERSION}coeffs.txt
xz casacore-data-igrf_$VERSION.orig.tar
